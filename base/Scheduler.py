from datetime import datetime, timezone
from apscheduler.schedulers.background import BackgroundScheduler
from .api import send
from .models import MailEvent, Client, Message
from django.db.models import Q
import pytz
from django.utils import timezone


def task():
    mail_list = MailEvent.objects.all()
    for mail in mail_list:
        if mail.message_set.count() == 0:
            now_in_timezone = datetime.now()
            if now_in_timezone > mail.datetime and now_in_timezone < mail.endtime:
                clients = Client.objects.filter(Q(phone_code=mail.filter_code) or Q(tag=mail.filter_tag))
                for clnt in clients:
                    msg = Message(mail_event = mail, client = clnt)
                    msg.save()
                    response = send(msg.id, clnt.number, mail.text)
                    msg.status = True
                    msg.save()
                    print(response)           

def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(task, 'interval', minutes=1)
    scheduler.start()