from django.db import models
import pytz
# Create your models here.
from django.db.models import Q

TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))


    
class Client(models.Model):

    number = models.IntegerField(("Номер клиента"))
    phone_code = models.IntegerField(("Код мобильного оператора"))
    tag = models.CharField(("Произвольная метка"), max_length=10)
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')

    def __str__(self):
        return str(self.number)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class MailEvent(models.Model):

    datetime = models.DateTimeField(("Дата и время запуска."), auto_now=False, auto_now_add=False)
    text = models.TextField(("Текст сообщения для доставки клиенту."))
    filter_code = models.IntegerField(("Код мобильного оператора"))
    filter_tag = models.CharField(("Фильтр по тегу"), max_length=10)
    endtime = models.DateTimeField(("Время окончания рассылки"), auto_now=False, auto_now_add=False)

    def __str__(self):
        return self.text

    class Meta:
        managed = True
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

class Message(models.Model):

    created = models.DateTimeField(("Дата отправки"), auto_now=False, auto_now_add=True)
    status = models.BooleanField(("Статус отправки"), default=False)
    mail_event = models.ForeignKey(MailEvent, verbose_name=("Рыссылка"), on_delete=models.CASCADE)
    client = models.ForeignKey(Client, verbose_name=("Клиент"), on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.created)

    class Meta:
        verbose_name = 'Messages'
        verbose_name_plural = 'Messagess'



# class Message(models.Model):

    # created = models.DateTimeField(("Дата и время отправки/создания"), auto_now=False, auto_now_add=True)
    # status = models.BooleanField(("Статус отправки"), default=False)
    # mail_event = models.ForeignKey(MailEvent, verbose_name=("Рассылка"), on_delete=models.CASCADE)
    # client = models.ForeignKey(Client, verbose_name=("Клиент"), on_delete=models.CASCADE)

#     def __str__(self):
#         return str(self.created)

#     class Meta:
#         verbose_name = 'Message'
#         verbose_name_plural = 'Messages'