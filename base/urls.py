from django.urls import path
from rest_framework import routers
from .api import MailEventViewSet, ClientViewSet, MessageViewSet
from . import views


router = routers.DefaultRouter()
router.register('api/mail_events', MailEventViewSet, 'MailEvent')
router.register('api/clients', ClientViewSet, 'Client')
router.register('api/messages', MessageViewSet, 'Messages')
# path('statistics/', StatisticsOverview, 'statistics'),

urlpatterns = router.urls 