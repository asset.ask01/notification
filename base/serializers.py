from rest_framework import serializers
from base.models import MailEvent, Client, Message

class MailEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailEvent
        fields = '__all__'

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

# class MessageSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Message
#         fields = '__all__'