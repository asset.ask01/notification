from base.models import MailEvent, Client, Message
from rest_framework import viewsets, permissions, status
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import MailEventSerializer, ClientSerializer, MessageSerializer
from datetime import datetime    
from django.db.models import Q
import requests

def send(msgId, num, text):
    headers = {
            'accept': 'application/json',
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDI0Nzk0NzEsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkFzc2FfY29tIn0.mTTNZUyltiqAhDwwlHE98lGGK-OQmUhXVd0CEUNe8GE',
            'Content-Type': 'application/json',
        }
    json_data = {
           'id': msgId,
           'phone': num,
           'text': text,
    }
    try:
        response = requests.post('https://probe.fbrq.cloud/v1/send/'+ str(msgId), headers=headers, json=json_data)
        return response
    except:
        return "Api not responding"



class MailEventViewSet(viewsets.ModelViewSet):
    queryset = MailEvent.objects.all()
    permission_classes = [
        permissions.AllowAny
        ]
    serializer_class = MailEventSerializer
    http_method_names = ['get', 'post', 'head', 'delete', 'put']

    filterset_fields = ['id', 'datetime', 'text', 'filter', 'endtime']


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        json_data = {
           'Code': 0,
           'Message': serializer.data['text'],
           'id': serializer.data['id'],
           'status': 'Created'
        }
        return Response(json_data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()
        MailEventViewSet.perform_send(serializer=serializer)

    def perform_send(serializer):
        if datetime.now() > datetime.strptime(serializer.data['datetime'], '%Y-%m-%dT%H:%M:%S') and datetime.now() < datetime.strptime(serializer.data['endtime'], '%Y-%m-%dT%H:%M:%S') :
            clients = Client.objects.filter(Q(phone_code=serializer.data['filter_code']) or Q(tag=serializer.data['filter_tag']))
            for clnt in clients:
                mail = MailEvent.objects.get(pk = serializer.data['id'])
                msg = Message(mail_event = mail, client = clnt)
                msg.save()
                response = send(msg.id, clnt.number, serializer.data['text'])
                msg.status = True
                msg.save()


    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        json_data = {
           'Code': 0,
           'Message': serializer.data['text'],
           'id': serializer.data['id'],
           'status': 'Updated'
        }
        return Response(json_data)


    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        json_data = {
           'Code': 0,
           'status': 'Deleted'
        }
        return Response(json_data, status=status.HTTP_204_NO_CONTENT)


    
    def put(self, request, *args, **kwargs):
        return super(MailEventViewSet, self).update(request, *args, **kwargs)

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    permission_classes = [
        permissions.AllowAny
        ]
    serializer_class = ClientSerializer
    http_method_names = ['get', 'post', 'head', 'delete', 'put']
    filterset_fields = ['id', 'number', 'phone_code', 'tag', 'timezone']
    
    def put(self, request, *args, **kwargs):
        return super(ClientViewSet, self).update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        json_data = {
           'Code': 0,
           'number': serializer.data['number'],
           'id': serializer.data['id'],
           'status': 'Created'
        }
        return Response(json_data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        json_data = {
           'Code': 0,
           'number': serializer.data['number'],
           'id': serializer.data['id'],
           'status': 'Updated'
        }
        return Response(json_data)


    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        json_data = {
           'Code': 0,
           'status': 'Deleted'
        }
        return Response(json_data, status=status.HTTP_204_NO_CONTENT)


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    permission_classes = [
        permissions.AllowAny
        ]
    serializer_class = MessageSerializer
    http_method_names = ['get', 'post', 'head', 'delete', 'put']
    filterset_fields = ['id', 'created', 'status', 'mail_event', 'client']
    

